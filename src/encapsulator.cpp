/*
 * encapsulator.cpp
 *
 *  Created on: Mar 30, 2018
 *      Author: walaryne
 */

#include "encapsulator.hpp"
#include <tuple>
#include <cstdlib>
#include <cstdio>
#include <cstring>

Encapsulator::Encapsulator(int header_length) {
	_header_length = header_length;
	_body_length = 0;
	_code_length = 2;
}

Encapsulator::~Encapsulator() {
	// TODO Auto-generated destructor stub
}

std::string Encapsulator::Encapsulate(std::string data, char keycode) {
	char header[_header_length + 1] = "";
	std::cout << "Raw Keycode Input: " << static_cast<int>(keycode) << std::endl;
	std::sprintf(header, "%2d%4d", static_cast<int>(keycode), static_cast<int>(data.length()));
	std::cout << "Raw Encapsulated Data: " << std::string(header) + data << std::endl;
	return std::string(header) + data;
}

std::pair<int, int> Encapsulator::Deencapsulate(std::string data) {
	char header[_header_length + 1] = "";
	std::cout << "Encapsulator Data Output: " << data << std::endl;
	int keycode = std::atoi(data.substr(0, _code_length).c_str());
	std::cout << "Encapsulator Keycode Output: " << keycode << std::endl;
	_body_length = std::atoi(data.substr(_code_length + 1).c_str());
	std::cout << "Encapsulator Remaining Length Output: " << _body_length << std::endl;
	return { _body_length, keycode };
}

