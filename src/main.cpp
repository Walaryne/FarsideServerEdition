/*
 ============================================================================
 Name        : FarsideServerEdition.cpp
 Author      : Walaryne
 Version     :
 Copyright   : (c) Walaryne, 2018
 Description : Farside's experimental serverside binary
 ============================================================================
 */

#include <iostream>
#include <string>
#include "server.hpp"

int main(int argc, char **argv) {
	std::string port;
	if(!(argc < 2) && !(argc > 2)) {
		port = std::string(argv[1]);
	} else {
		exit(1);
	}
	Server s(port);
	s.Run();
}
