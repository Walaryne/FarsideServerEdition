/*
 * encapsulator.hpp
 *
 *  Created on: Mar 30, 2018
 *      Author: walaryne
 */

#ifndef ENCAPSULATOR_HPP_
#define ENCAPSULATOR_HPP_
#include <iostream>
#include <sstream>

class Encapsulator {
public:
	Encapsulator(int);
	virtual ~Encapsulator();
	std::string Encapsulate(std::string, char);
	std::pair<int, int> Deencapsulate(std::string);
private:
	int _body_length;
	int _header_length;
	int _code_length;
};

#endif /* ENCAPSULATOR_HPP_ */
