/*
 * server.hpp
 *
 *  Created on: Mar 30, 2018
 *      Author: walaryne
 */

#ifndef SERVER_HPP_
#define SERVER_HPP_
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <iostream>
#include <string.h>
#include "encapsulator.hpp"

class Server {
public:
	Server(std::string);
	virtual ~Server();
	void Run();
private:
	fd_set _master;
	fd_set _read_fds;
	int _fdmax;
	int _listener;
	int _newfd;
	sockaddr_storage _remoteaddr;
	socklen_t _addrlen;
	char _buf[512];
	int _nbytes;
	char _remoteIP[INET6_ADDRSTRLEN];
	int _yes = 1;
	const char *_port;
	addrinfo _hints, *_ai, *_p;
};

#endif /* SERVER_HPP_ */
