/*
 * dbmanager.cpp
 *
 *  Created on: Mar 17, 2018
 *      Author: walaryne
 */

#include "dbmanager.hpp"

DatabaseManager::DatabaseManager() {
	db = nullptr;
	zErrMsg = nullptr;
	rc = 0;
	sql = nullptr;
	openedFlag = 0;
}

DatabaseManager::~DatabaseManager() {
	sqlite3_close(db);
}

void DatabaseManager::Open(std::string filename) {
	if(!openedFlag) {
		rc = sqlite3_open(filename.c_str(), &db);
		if(rc) {
			std::cout << "Errored while trying to open " << filename << "\n" << std::endl;
		} else {
			std::cout << filename << " successfully opened." << "\n" << std::endl;
			openedFlag = true;
		}
	} else {
		std::cout << "A database has already been opened, close it first." << "\n" << std::endl;
	}
}

void DatabaseManager::LoadStatement(char *statement) {
	sql = statement;
}

void DatabaseManager::Execute(int callback(void *data, int argc, char **argv, char **azColName)) {
	if(!openedFlag) {
		std::cout << "A database hasn't been opened yet, open one first." << "\n" << std::endl;
	} else {
		sqlite3_exec(db, sql, callback, (void*)data, &zErrMsg);
		if(rc != SQLITE_OK) {
			fprintf(stderr, "SQL error: %s\n", zErrMsg);
			sqlite3_free(zErrMsg);
		} else {
			fprintf(stdout, "Operation done successfully\n");
		}
	}
}

void DatabaseManager::AggregateQueue(std::string statement) {
	queue.push_back(statement);
}

void DatabaseManager::ExecuteQueue(int callback(void *data, int argc, char **argv, char **azColName)) {
	if(!openedFlag) {
		std::cout << "A database hasn't been opened yet, open one first." << "\n" << std::endl;
	} else {
		while(!queue.empty()) {
			const char *queuedstatement;
			queuedstatement = queue.front().c_str();
			sqlite3_exec(db, queuedstatement, callback, (void*)data, &zErrMsg);
			if(rc != SQLITE_OK) {
				fprintf(stderr, "SQL error: %s\n", zErrMsg);
				sqlite3_free(zErrMsg);
			} else {
				fprintf(stdout, "Operation done successfully\n");
			}
			queue.pop_front();
		}
	}
}

