/*
 * usermanager.cpp
 *
 *  Created on: May 1, 2018
 *      Author: walaryne
 */

#include "usermanager.hpp"
#include "dbmanager.hpp"
#include <uuid/uuid.h>

DatabaseManager mgr;

char sanitizable[] = { "';\"*=" };

Usermanager::Usermanager() {
	// TODO Auto-generated constructor stub

}

Usermanager::~Usermanager() {
	// TODO Auto-generated destructor stub
}

std::string impeller(std::string string) {
	return "'" + string + "'";
}

void sanitizer(std::string &string) {
	for(int i = 0; i <= (int) sizeof sanitizable; i++) {
		int pos = 0;
		char k = sanitizable[i - 1];
		while(string.find(k) != std::string::npos) {
			pos = string.find(k, pos);
			string.erase(pos, 1);
		}
		pos = 0;
	}
}

std::string makeuuid() {
	uuid_t id;
	uuid_generate(id);

	char *string = new char[100];
	uuid_unparse(id, string);

	return std::string(string);
}

static int callback(void *data, int argc, char **argv, char **azColName) {
	int i;
	fprintf(stderr, "%s: ", (const char*)data);
	for(i=0; i < argc; i++) {
		printf("%s = %s\n", azColName[i], argv[i] ? argv[i] : "NULL");
	}
	printf("\n");
	return 0;
}

void Usermanager::Initialize() {
	mgr.Open("users.db");
}

bool Usermanager::ParseStringCommand(int keycode, std::string command) {
	//If it's a user registration message
	if(keycode == 0x02) {
		std::string password, username;
		//Perform basic checks, and register the user into the database
		if(command.find(" ", 0) == std::string::npos) {
			//Command string was malformed
			return false;
		} else {
			//SQL Injection risk has been minimized by the new sanitizer, but it could
			//still be unsafe!
			username = command.substr(0, command.find(" ", 0));
			password = command.substr(command.find(" ", 0) + 1);
			sanitizer(username);
			sanitizer(password);
			std::string sql =
					std::string("insert into users ") +
					std::string("values") +
					std::string("(") +
					impeller(username) +
					std::string(", ") +
					impeller(password) +
					std::string(", ") +
					std::string("'user', ") +
					impeller(makeuuid()) +
					std::string(");");
			mgr.AggregateQueue(sql);
			mgr.ExecuteQueue(callback);
			return true;
		}
	} else {
		//invalid keycode
		return false;
	}
}

