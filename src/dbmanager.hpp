/*
 * dbmanager.hpp
 *
 *  Created on: Mar 17, 2018
 *      Author: walaryne
 */

#ifndef DBMANAGER_HPP_
#define DBMANAGER_HPP_
#include <sqlite3.h>
#include <iostream>
#include <deque>

class DatabaseManager {
public:
	DatabaseManager();
	virtual ~DatabaseManager();
	void Open(std::string);
	void LoadStatement(char *);
	void Execute(int(*)(void *, int, char**, char**));
	void AggregateQueue(std::string);
	void ExecuteQueue(int(*)(void *, int, char**, char**));
private:
	sqlite3 *db;
	char *zErrMsg;
	int rc;
	char *sql;
	bool openedFlag;
	const char* data = "Callback function called";
	std::deque<std::string> queue;
};

#endif /* DBMANAGER_HPP_ */
