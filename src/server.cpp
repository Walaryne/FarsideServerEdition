/*
 * server.cpp
 *
 *  Created on: Mar 30, 2018
 *      Author: walaryne
 */

#include "server.hpp"
#include <sys/socket.h>
#include <unistd.h>
#include <tuple>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include "usermanager.hpp"

int header_length = 6;

Encapsulator e(header_length);
Usermanager um;

void *get_in_addr(struct sockaddr *sa) {
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
	}
	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

Server::Server(std::string port) {
	_fdmax = 0;
	_listener = 0;
	_newfd = 0;
	_addrlen = 0;
	_nbytes = 0;
	_ai = nullptr;
	_p = nullptr;
	_port = port.c_str();
}

Server::~Server() {
	// TODO Auto-generated destructor stub
}

void Server::Run() {
	um.Initialize();
	int i, j, rv;
	FD_ZERO(&_master);
	FD_ZERO(&_read_fds);
	memset(&_hints, 0, sizeof _hints);
    _hints.ai_family = AF_UNSPEC;
    _hints.ai_socktype = SOCK_STREAM;
    _hints.ai_flags = AI_PASSIVE;
    if ((rv = getaddrinfo(NULL, _port, &_hints, &_ai)) != 0) {
        fprintf(stderr, "selectserver: %s\n", gai_strerror(rv));
        exit(1);
    }
    for(_p = _ai; _p != NULL; _p = _p->ai_next) {
        _listener = socket(_p->ai_family, _p->ai_socktype, _p->ai_protocol);
        if (_listener < 0) {
            continue;
        }

        setsockopt(_listener, SOL_SOCKET, SO_REUSEADDR, &_yes, sizeof(int));

        if (bind(_listener, _p->ai_addr, _p->ai_addrlen) < 0) {
            close(_listener);
            continue;
        }

        break;
    }
    if (_p == NULL) {
        fprintf(stderr, "selectserver: failed to bind\n");
        exit(2);
    }
    freeaddrinfo(_ai);
    if (listen(_listener, 10) == -1) {
        perror("listen");
        exit(3);
    }
    // add the listener to the master set
    FD_SET(_listener, &_master);

    // keep track of the biggest file descriptor
    _fdmax = _listener; // so far, it's this one

    // main loop
    for(;;) {
        _read_fds = _master; // copy it
        if (select(_fdmax+1, &_read_fds, NULL, NULL, NULL) == -1) {
            perror("select");
            exit(4);
        }

        // run through the existing connections looking for data to read
        for(i = 0; i <= _fdmax; i++) {
            if (FD_ISSET(i, &_read_fds)) { // we got one!!
                if (i == _listener) {
                    // handle new connections
                    _addrlen = sizeof _remoteaddr;
                    _newfd = accept(_listener,
                        (struct sockaddr *)&_remoteaddr,
                        &_addrlen);

                    if (_newfd == -1) {
                        perror("accept");
                    } else {
                        FD_SET(_newfd, &_master); // add to master set
                        if (_newfd > _fdmax) {    // keep track of the max
                            _fdmax = _newfd;
                        }
                        printf("selectserver: new connection from %s on "
                            "socket %d\n",
                            inet_ntop(_remoteaddr.ss_family,
                                get_in_addr((struct sockaddr*)&_remoteaddr),
                                _remoteIP, INET6_ADDRSTRLEN),
                            _newfd);
                    }
                } else {
                    // handle data from a client
                	// pull the header
                    if ((_nbytes = recv(i, _buf, header_length, 0)) <= 0) {
                        // got error or connection closed by client
                        if (_nbytes == 0) {
                            // connection closed
                            printf("selectserver: socket %d hung up\n", i);
                        } else {
                            perror("recv");
                        }
                        close(i); // bye!
                        FD_CLR(i, &_master); // remove from master set
                    } else {
                        // we got some data from a client
                    	// get the remaining bytes to an int and keycode for type
                    	auto retrvalue = e.Deencapsulate(_buf);
                    	int remaining = retrvalue.first;
                    	int keycode = retrvalue.second;
                    	std::cout << "Keycode: " << keycode << std::endl;
                    	memset(&_buf, 0, sizeof _buf);
                    	// attempt to receive the remaining bytes
                    	recv(i, _buf, remaining, 0);
                    	std::cout << _buf << std::endl;
                    	if(keycode == 0x01) {
                    		for(j = 0; j <= _fdmax; j++) {
                    			// send to everyone!
                    			if (FD_ISSET(j, &_master)) {
                    				// except the listener and ourselves
                    				if (j != _listener && j != i) {
                    					std::string newbuffer;
                    					newbuffer = e.Encapsulate(_buf, 0x01);
                    					memset(&_buf, 0, sizeof _buf);
                    					if (send(j, newbuffer.c_str(), newbuffer.length(), 0) == -1) {
                    						perror("send");
                    					}
                    				}
                    			}
                    		}
                    	} else {
                    		um.ParseStringCommand(keycode, _buf);
                    		memset(&_buf, 0, sizeof _buf);
                    	}
                    }
                } // END handle data from client
            } // END got new incoming connection
        } // END looping through file descriptors
    } // END for(;;)--and you thought it would never end!
}

