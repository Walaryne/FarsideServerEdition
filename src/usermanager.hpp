/*
 * usermanager.hpp
 *
 *  Created on: May 1, 2018
 *      Author: walaryne
 */

#ifndef USERMANAGER_HPP_
#define USERMANAGER_HPP_
#include <uuid/uuid.h>
#include <sqlite3.h>
#include <iostream>

class Usermanager {
public:
	Usermanager();
	virtual ~Usermanager();
	bool ParseStringCommand(int, std::string);
	void Initialize();
private:
	bool CreateUser();
	bool DeleteUser();
	bool KickUser();
	bool BanUser();
	bool ChangeUsername();
	bool Login();
	char *_string = new char[100];
	uuid_t _uuid;
};

#endif /* USERMANAGER_HPP_ */
